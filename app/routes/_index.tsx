import type { MetaFunction } from '@remix-run/node';
import { columns } from '~/components/columns';
import { DataTable } from '~/components/data-table';
import { UserNav } from '~/components/user-nav';
import tasks from '~/data/tasks.json';
import logs from '~/data/logs.json';

export const meta: MetaFunction = () => {
  return [{ title: 'Lokillo' }, { name: 'tbd', content: 'tbd' }];
};

export default function Index() {
  return (
    <div style={{ fontFamily: 'system-ui, sans-serif', lineHeight: '1.8' }}>
      <div className="hidden h-full flex-1 flex-col space-y-8 p-8 md:flex">
        <div className="flex items-center justify-between space-y-2">
          <div>
            <h2 className="text-2xl font-bold tracking-tight">Lokillo</h2>
            <p className="text-muted-foreground">Here&apos;s a list of your pending tasks!</p>
          </div>
          <div className="flex items-center space-x-2">
            <UserNav />
          </div>
        </div>
        <DataTable data={tasks} columns={columns} />
        {/* <DataTable data={logs} columns={columns} /> */}
      </div>
    </div>
  );
}
