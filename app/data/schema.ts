import { z } from 'zod';

export const taskSchema = z.object({
  id: z.string(),
  title: z.string(),
  status: z.string(),
  label: z.string(),
  priority: z.string(),
});

export const logSchema = z.object({
  id: z.string(),
  labels: z.object({
    app: z.string(),
    context: z.string(),
    exception: z.string(),
    timestampt: z.string(),
    content: z.object({}),
  }),
  line: z.string(),
  timestampt: z.string(),
});

export type Task = z.infer<typeof taskSchema>;
export type Log = z.infer<typeof logSchema>;
